
// +----------------------------------------------------------------------
// | CmPage [ 通用页面框架 ]
// +----------------------------------------------------------------------
// | Licensed under the Apache License, Version 2.0
// +----------------------------------------------------------------------
// | Author: defans <defans@sina.cn>
// +----------------------------------------------------------------------

/**
 本网关程序需要单独调用，
 安装模块：npm install http-proxy
 启动： pm2 node gate.js

 @module gate
 */

/**
 * 网关程序功能，实现了http根据规则进行代理转发的功能，主要规则如下：
 * 1、默认转向主系统
 * 2、根据modulename（业务模块名称）来判断转向哪个业务子系统
 */

const http = require('http');
const httpProxy = require('http-proxy');
const querystring = require('querystring');
const url = require('url');

// 新建一个代理 Proxy Server 对象
let proxy = httpProxy.createProxyServer({ changeOrigin: true, preserveHeaderKeyCase: true }); 
 
// 捕获异常
proxy.on('error', function (err, req, res) {
       res.writeHead(500, {
               'Content-Type': 'text/plain'  
            });  
            res.end('Something went wrong.');
        }); 


/**
 * 另外新建一个 HTTP 3100 端口的服务器，也就是常规 Node 创建 HTTP 服务器的方法。
 * 在每次请求中，根据转发规则，调用 proxy.web(req, res config) 方法进行请求分发
 */
let visitCnt = 0;
let server = http.createServer(function(req, res) {
        // 在这里可以自定义你的路由分发  
        let parms = querystring.parse(url.parse(req.url).query);
        console.log(parms);        
        //var host = req.headers.host, ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;  
        visitCnt ++;
        console.log("cnt: "+visitCnt+", cookie: " + req.headers.cookie);    
        if(parms['modulename'] ){
            proxy.web(req, res, getTarget(parms['modulename']));
        }else{
            proxy.web(req, res, { target: 'http://10.9.35.41:8380' });
        }

    }
);
console.log("listening on port 3100");
server.listen(3100);


/**
 * 定义业务模块所属的站点地址，也可以从主系统的URL接口取如下结构的JSON数据
 * 根据模块名称进行匹配，找出转向地址
 */
const moduleTargets = [{
        modules:',DocuListLookup,DocuList,DcouRecArrive,DocuRecBill,DocuRecOrder,DocuRecOrderApply,DocuRecPick,',       //列出了部分模块
        target: 'http://10.9.35.41:8380'
    },{
        modules:',ExamMarker,Exam,ExamRec,ExamStudent,Crontab,',       //列出了部分模块
        target: 'http://127.0.0.1:8380'
    }
];
function getTarget(modulename){
    for(let mod of moduleTargets){
        if(mod.modules.indexOf(','+modulename+',') >-1){
            return {target: mod.target };
        }
    }
    return { target: 'http://10.9.35.41:8380' };  
}