'use strict';
// +----------------------------------------------------------------------
// | CmPage [ 通用页面框架 ]
// +----------------------------------------------------------------------
// | Licensed under the Apache License, Version 2.0
// +----------------------------------------------------------------------
// | Author: defans <defans@sina.cn>
// +----------------------------------------------------------------------

/**
 * 选择其他系统的模块加入本系统，在 t_module_sys 增加关联记录，用于统一存放模块设置
 * -----------------------考虑到模块复制也不是很麻烦，目前暂时不用本功能-------------------------------
 */
const CMPage = require('../cmpage/page_lookup.js');

module.exports = class extends CMPage {
    /**
     * 取查询项的设置，结合POST参数，得到Where字句
     */
    async getQueryWhere() {
        //通过父类的方法取查询列设置解析后的where子句
        let where = await super.getQueryWhere();
        //此处增加额外的条件
        where += ` and id not in(select c_module from t_module_sys where c_sys='${cmpage.config.sys_name}')`; //也可以在查询列设置一条 ‘固定’类型的查询列，备注中填： c_status<>-1

        //cmpage.warn('v的点点滴滴多大奥付 发顺丰');

        return where;
    }


    /**
     * 
     * @method  getNameById
     * @return {string}  客户名称
     * @param {int} id  客户ID
     */
    async moduleSysAdd(ids) {
        let sql = `insert t_module_sys()`

    }

}